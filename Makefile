CC_SYS_FLAGS =
LIB_DIR = lib/
OBJ_DIR = objects/
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ABS_DIR := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

BUILD_DIRS = impl
BUILD_DYN = libSWTP.so
BUILD_STATIC = libSWTP.a

DYN_LIB_FLAGS = -L./ -L./dyn/ -Wl,-R./ -Wl,-R./dyn/
DYN_LIB_ARGS = $(patsubst dyn/lib%.so, -l% , $(BUILD_DYN))

LIB_CLASSES = 
PCH =
OBJS += $(patsubst impl/%.cpp,objects/impl/%.o,$(shell find impl/ -type f -name '*.cpp'))
OBJS += ext/pSocket.cpp ext/xxhash.c
OBJS += $(patsubst lib/%.cpp,objects/lib/%.o,$(shell find lib/ -type f -name '*.cpp'))

#PCH += $(LIB_PCH:%.h=$(LIB_DIR)%.h.gch)
#OBJS += $(LIB_CLASSES:%.cpp=$(OBJ_DIR)$(LIB_DIR)%.o)

FC_LIBS = 

CC = g++ -std=c++0x -fms-extensions $(CC_SYS_FLAGS)
OLDCC = g++ $(CC_SYS_FLAGS)

OPTI = -mtune=native -march=native -O3
debug: CCPLUS = $(OPTI) -g -DDEBUG
release: CCPLUS = $(OPTI)  -DOPTIMIZE

CFLAGS = -Wl,--no-as-needed -pipe -c -I$(LIB_DIR) -I./ -I./impl/ $(CCPLUS) -MMD -MP
BFLAGS = -pipe -c $(CCPLUS)
CFLAGSNW = $(CFLAGS)
CFLAGS += -Wall -Wextra
LFLAGS = -Wall -Wextra -pipe -I$(LIB_DIR) -I./ -I./impl/ $(CCPLUS) -MMD -MP
ifeq ($(OS),Windows_NT)
PIC = 
else
PIC = -fPIC
endif
#release: LFLAGS += -Lext/static-linux

LIBS +=
debug:   LFLAGS +=
release: LFLAGS +=

debug : $(PCH) 
release : $(PCH) 

#dependent builds
.cpp.o: $(PCH)
	$(CC) $(CFLAGS) $(PIC)  $< -o $@

$(OBJ_DIR)%.o: %.cpp %.h
	@mkdir -p "$(@D)"
	$(CC) $(CFLAGS) $(PIC) $< -o $@

debug: $(BUILD_DYN) test
release: $(BUILD_DYN) test
	
libSWTP.so: $(OBJS)
	g++ -shared -Wl,-soname,libSWTP.so -o libSWTP.so $(OBJS)
libSWTP.a : $(OBJS)
	ar rvs libSWTP.a $(OBJS)
	
test : test/testMain.cpp $(BUILD_DIRS) $(BUILD_DYN)
	$(CC) $(LFLAGS) $(DYN_LIB_FLAGS) $(DYN_LIB_ARGS) $(FC_LIBS) $< -o test

#secondary instructions
clean:
	rm -r objects/*
	
install: 
	

uninstall:


-include $(OBJFILES:.o=.d)
.FORCE: