/* 
 * File:   Stream.h
 * Author: Galen Swain
 *
 * Created on October 7, 2015, 3:46 PM
 */

#ifndef SWTP_STREAM_H
#define	SWTP_STREAM_H

namespace SWTP {
class Stream {
public:
    Stream();
    Stream(const Stream& orig);
    virtual ~Stream();
    
    
private:

};
};

#endif	/* STREAM_H */

