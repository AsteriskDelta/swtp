/* 
 * File:   Socket.h
 * Author: Galen
 *
 * Created on October 8, 2015, 4:07 PM
 */

#ifndef SWTP_SOCKET_H
#define	SWTP_SOCKET_H

#include "ext/pSocket.h"
namespace SWTP {
    
class Socket {
public:
    Socket();
    Socket(const Socket& orig);
    virtual ~Socket();
private:
    UDPSocket *sock;
};
};

#endif	/* SOCKET_H */

