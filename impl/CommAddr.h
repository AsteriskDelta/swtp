/* 
 * File:   CommAddr.h
 * Author: Galen
 *
 * Created on October 8, 2015, 4:55 PM
 */

#ifndef SWTP_COMMADDR_H
#define	SWTP_COMMADDR_H
#include "include.h"
#include <string>
#include <cstring>
#include "include.h"
namespace SWTP {

struct CommAddr {
    std::string addr;
    Uint16 port;
    bool operator==(const CommAddr &o) const {
        return (addr == o.addr && port == o.port);
    }
};

namespace std {
  template <> struct hash<CommAddr> {
    std::size_t operator()(const CommAddr& k) const {
      using std::size_t; using std::hash;  using std::string;
      return (hash<string>()(k.addr) ^ port);
    }
  };

}
};
#endif	/* COMMADDR_H */

