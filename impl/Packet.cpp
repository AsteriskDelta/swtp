/* 
 * File:   Packet.cpp
 * Author: Galen
 * 
 * Created on October 8, 2015, 4:14 PM
 */

#include "Packet.h"
#include "ext/xxhash.h"
namespace SWTP {
Packet::Packet() {
}

Packet::~Packet() {
}


void Packet::setData(const char* reqData, unsigned int reqLength) {
  this->cleanup();
  
  //std::cout << "rLen: " << reqLength << "\n";
  extended = (reqLength > PACKET_BSC_LEN);
  //if(extended) std::cout << "ext\n";
  if(extended) {
    ext.length = reqLength;
    ext.data = reqData;
  } else {
    bsc.length = (unsigned char)reqLength;
    bsc.data = reqData;
  }
  
  hash = this->calcHash();
}

unsigned int Packet::calcHash() const {
  const unsigned int length = this->getDataLength();
  const char* data = this->getData(); unsigned int tmpHash = 0;
  if(data == NULL || length == 0) {
    tmpHash = 0; return tmpHash;
  } else tmpHash = XXH32(data, length, PACKET_CHECKSUM_SEED);
  return tmpHash;
}

void Packet::cleanup() {
  if(received && this->getData() != NULL) delete[] this->getData();
}
};
