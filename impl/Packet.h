/* 
 * File:   Packet.h
 * Author: Galen
 *
 * Created on October 8, 2015, 4:14 PM
 */

#ifndef SWTP_PACKET_H
#define	SWTP_PACKET_H
#include "include.h"

//Maximum message length of 32MB
#define PACKET_ALLOC_LEN 1024*16
#define PACKET_MAX_LEN (32 * 1024 * 1024)
#define PACKET_CHECKSUM_SEED 0x3EFC0217
#define PACKET_BSC_LEN 255
#define PACKET_EXT_LEN 4294967295

#define PACKET_MAGIC_BEG ((char)0x6E)
#define PACKET_MAGIC_END ((char)0xC7)

#define PACKET_BSC_SIZEOF sizeof(unsigned char)
#define PACKET_EXT_SIZEOF sizeof(unsigned int)
//May be any value, as long as it's identical across releases
#define PACKET_HASH_CMD_PREFIX 0xCF36

#define PACKET_REPEATED_DATA_MAX 8192
#define PACKET_INTRIN_CMD_MAX 128

namespace SWTP {

class Packet {
public:
    Packet();
    Packet(const Packet& orig);
    virtual ~Packet();
    struct {
        unsigned char magicBegin : 8;
        bool hybrid       : 1;//Should always be 0x1
        bool extended 	  : 1;//Denotes active union: bsc if false, ext if true
        bool received	  : 1;//Was this command composed here, or should we delete data on destruction? Set by API
        bool isReturn	  : 1;//If this packet denotes a request return status
        unsigned int packetID : 20;//Packet ID for requesting error correction, set by API
        //unsigned int padding : 8;//Force byte alignment on systems with crazy compiler constraints (ie, no support for addressing of anon struct)
    };		
    unsigned int hash;
    unsigned char magicEnd;
    protected: union {
        struct {//Default
          union{ const char *data; char *ncData; };
          unsigned char length; unsigned char lengthPadd[3];
        } bsc;
        struct {//Extended
          union{ const char *data; char *ncData; };
          unsigned int length;
        } ext;     
    };
public:
    void setData(const char* reqData, unsigned int reqLength);
    inline void setData(const unsigned char* reqData, unsigned int reqLength);
    inline const char* getData() const;
    inline char* getDataDyn();

    inline unsigned int getHeaderLength() const;
    inline unsigned int getTotalLength() const;
    inline unsigned int getDataLength() const;

    inline unsigned int getID() const;

    unsigned int calcHash() const;
protected:
    void cleanup();
};

//Return difference in bytes (chars)
inline unsigned int Packet::getHeaderLength() const {
  return (unsigned int) ((char*)&(this->magicEnd)+sizeof(Uint8) - (char*)this);
}

inline unsigned int Packet::getTotalLength() const {
  return this->getHeaderLength() + 
  (extended? (PACKET_EXT_SIZEOF + ext.length) : (PACKET_BSC_SIZEOF + bsc.length));
}

unsigned int Packet::getDataLength() const {
  return extended? ext.length : bsc.length;
}

const char* Packet::getData() const {
  return extended? ext.data : bsc.data;
}

char* Packet::getDataDyn() {
  return extended? ext.ncData : bsc.ncData;
}

void Packet::setData(const unsigned char* reqData, unsigned int reqLength) {
  this->setData((const char*)reqData, reqLength);
}

inline unsigned int Packet::getID() const {
  return this->packetID;
}
};

#endif	/* PACKET_H */

