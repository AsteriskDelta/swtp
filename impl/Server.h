/* 
 * File:   Server.h
 * Author: Galen Swain
 *
 * Created on October 7, 2015, 3:47 PM
 */

#ifndef SWTP_SERVER_H
#define	SWTP_SERVER_H
#include "include.h"
#include "Socket.h"
#include "Client.h"

namespace SWTP {
class Server {
public:
    Server();
    Server(Uint16 nPort, std::string addr = "");
    virtual ~Server();
    
    bool start();
    void stop();
    
    inline operator bool() const;
    Client* accept(bool async = true);
private:
    Uint16 port; std::string address;
    Socket *srvSocket;
    
    bool mkSocket(Uint16 nPort, std::string addr);
};

inline Server::operator bool() const {
    return srvSocket != NULL;
}

};

#endif	/* SERVER_H */

