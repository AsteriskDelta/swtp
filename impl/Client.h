/* 
 * File:   Client.h
 * Author: Galen Swain
 *
 * Created on October 7, 2015, 3:49 PM
 */

#ifndef SWTP_CLIENT_H
#define	SWTP_CLIENT_H
#include "Socket.h"
#include "include.h"

namespace SWTP {
class Client {
public:
    Client();
    Client(Socket *sock);
    virtual ~Client();
    
    
protected:
    bool isRemote;
    std::string address;
    Uint16 port;
    Socket *socket;
};
};
#endif	/* CLIENT_H */

